# Batch-sof Tools #

This repo contains a few python scripts which make accessing SOF Olympiad results easier.

### Installation ###

1. Clone this repo.
2. Get the following dependencies: BeautifulSoup
3. Profit!

### Usage ###

* **sof.py** - Deprecated. Use sof2.py
* **sof2.py** - Get results of single school.
* **schoolfind.py** - Find school codes.
* **temp.py** - Ignore this file.

## sof2.py ##

* Run python script with `python sof2.py`
* Enter Grade: Number from 1 to 12.
* Enter School code: In this format - AA0123 (For code, run schoolfind.py)
* Enter Range: Number (1-999)
* Roll numbers in range 0-range will be searched.

#### Output: ####

* **log.txt** - For reference
* **pretty.txt** - Final json output.

### Who do I talk to? ###
For any questions / details / bug reports, create an issue in the tracker.