import json

jsondump = ['{"Parent\'s Name:": "SANDEEP CHAUDHARY", "School Award Won:": "Participation Certificate", "International Rank:": "862", "Student Name:": "ADITYA KUMAR CHAUDHARY", "State / Zonal Rank:": "379", "Roll No.:": "KA0466-10-001", "School Rank:": "24", "Total Marks Scored:": "21", "School:": "DELHI PUBLIC SCHOOLBANGALOREKARNATAKA", "Class:": "10", "Qualified for 2nd round:": "NO", "State / Zonal Award Won:": "N.A.", "International Award Won:": "N.A."}',
 '{"Parent\'s Name:": "DEBARANJAN SWAIN", "School Award Won:": "Participation Certificate", "International Rank:": "872", "Student Name:": "ANKIT SWAIN", "State / Zonal Rank:": "384", "Roll No.:": "KA0466-10-004", "School Rank:": "25", "Total Marks Scored:": "21", "School:": "DELHI PUBLIC SCHOOLBANGALOREKARNATAKA", "Class:": "10", "Qualified for 2nd round:": "NO", "State / Zonal Award Won:": "N.A.", "International Award Won:": "N.A."}',
 '{"Parent\'s Name:": "SUBHANKAR CHAKRABARTI", "School Award Won:": "Participation Certificate", "International Rank:": "333", "Student Name:": "SHUBHAYU CHAKRABARTI", "State / Zonal Rank:": "125", "Roll No.:": "KA0466-10-009", "School Rank:": "12", "Total Marks Scored:": "34", "School:": "DELHI PUBLIC SCHOOLBANGALOREKARNATAKA", "Class:": "10", "Qualified for 2nd round:": "NO", "State / Zonal Award Won:": "N.A.", "International Award Won:": "N.A."}']

jd2 = json.dumps('[' + ','.join(jsondump) +']')
print jd2.decode('string_escape')

print "\n\n"

print jd2.decode('string_escape').replace("Parent\'s","Parent")

#[json.dumps(el) for el in jsondump]