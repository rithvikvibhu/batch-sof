import requests
import json
import pprint
from bs4 import BeautifulSoup

jsondumpFinal = []
oldschool = ""
logfile = open("logschool.txt", "a")

## Debug
grade = 5
rolllist = range(20)
school = range(1200,5000,1)
print school

## Main

for schindex in school:
    schi = schindex+1
    schi = '%04d' % schi
    fullschcode = "KA{}".format(schi)
    #print "School (padded): ", schi         # 0004
    print "Full schoolcode: ", fullschcode   # KA0004
    
    for index in rolllist:
    	i = index+1
    	i = '%03d' % i
    	payload = {"action":"result", "rollnumber1":fullschcode, "rollnumber2":grade, "rollnumber3":i}
    	r = requests.post("http://server1.sofworld.org/nco-result/show.php", data = payload)
    	
    	soup = BeautifulSoup(r.text, "html5lib")                                                            # Soup up html
    	table_data = [[cell.text for cell in row("td")]
    		for row in BeautifulSoup(r.text, "html5lib")("tr")]
    		
    	jsondump = json.dumps(dict(table_data))                 # Create json object
    	data = json.loads(jsondump)                             # Load json into data
    	if 'School:' in data and oldschool != data["School:"]:
		    oldschool = data["School:"]
		    lineToWrite = fullschcode + " - " + data["School:"] + "\n"
		    print lineToWrite
		    logfile.write(lineToWrite)
		    break


logfile.close()                     # Close log.txt
exit()