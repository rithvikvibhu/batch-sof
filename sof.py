import requests
import json
import pprint
from bs4 import BeautifulSoup


## Init
#payload = {"action":"result", "rollnumber1":"KA0466", "rollnumber2":"10", "rollnumber3":"073"}         # Sample Payload
jsondumpFinal = []
logfile = open("log.txt", "w")
prettyfile = open("pretty.txt", "w")


for index in xrange(100):
    i = index+1
    i = '%03d' % i
    payload = {"action":"result", "rollnumber1":"KA0466", "rollnumber2":"10", "rollnumber3":i}          # Generate payload
    r = requests.post("http://server1.sofworld.org/nco-result/show.php", data = payload)                # Get html
    soup = BeautifulSoup(r.text, "html5lib")                                                            # Soup up html
    table_data = [[cell.text for cell in row("td")]
        for row in BeautifulSoup(r.text, "html5lib")("tr")]
    
    jsondump = json.dumps(dict(table_data))                 # Create json object
    data = json.loads(jsondump)                             # Load json into data
    if 'Class:' in data:                                    # Check if valid record
        jsondumpFinal.append(jsondump)                      # Add record to jsondumpFinal
        print jsondump
        print ("\r\n")

#pprint.pprint(jsondumpFinal)       # Print final
#print type(jsondumpFinal)          # Print type of jsondumpFinal

for item in jsondumpFinal:
  logfile.write("%s\n" % item)      # Log to log.txt

print "\n\n"
jd2 = json.dumps('[' + ','.join(jsondumpFinal) +']').decode('string_escape').replace("Parent\'s","Parent")
print jd2[1:-1]                     # Remove trailing quotes

prettyfile.write(jd2[1:-1])         #Log to pretty.txt

logfile.close()                     # Close log.txt
prettyfile.close()                  # Close pretty.txt

exit()